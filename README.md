# Sliding Puzzle with Rust and Macroquad

![Sliding Puzzle](https://freemikan.com/blob/sliding_puzzle_1.gif)

## ビルドと実行

```console
$ git clone https://codeberg.org/freemikan/sliding_puzzle_macroquad.git
$ cd sliding_puzzle_macroquad
$ cargo run
```
