use macroquad::prelude::*;
use macroquad::rand::gen_range;

const SCR_W: f32 = 800.;
const SCR_H: f32 = 600.;
const COLS: usize = 4;
const ROWS: usize = 4;
const NUM_CELLS: usize = COLS * ROWS;

fn window_conf() -> Conf {
    Conf {
        window_title: "Sliding Puzzle with Macroquad".to_string(),
        window_width: SCR_W as i32,
        window_height: SCR_H as i32,
        ..Default::default()
    }
}

#[derive(Default, Clone)]
struct Cell {
    number: u32,
    number_text: String,
    delta: Vec2,
}

type CellArray = [[Cell; COLS]; ROWS];

struct SlidingAnimation {
    col: usize,
    row: usize,
    dir: Option<Direction>,
    speed: f32,
    progress: f32,
}

struct Board {
    position: Vec2,
    size: Vec2,
    border_thickness: f32,
    border_color: Color,
    cell_size: Vec2,
    cell_spacing: f32,
    cells: [[Cell; COLS]; ROWS],
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Direction {
    Left,
    Right,
    Up,
    Down,
}

impl Direction {
    fn opposite(self) -> Direction {
        match self {
            Direction::Left => Direction::Right,
            Direction::Right => Direction::Left,
            Direction::Up => Direction::Down,
            Direction::Down => Direction::Up,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum MenuItem {
    Reset,
    NewGame,
    Solve,
}

struct Menu {
    item: MenuItem,
    label: String,
    position: Vec2,
    size: Vec2,
}

#[derive(Debug, PartialEq, Clone, Copy)]
enum Phase {
    Playing,
    Reset,
    NewGame,
    Solve,
    BrandNewGame,
}

struct Game {
    phase: Phase,
    board: Board,
    sliding_animation: SlidingAnimation,
    shuffling_max: u32,
    shuffling_count: u32,
    track_of_moves: Vec<Direction>,
    menus: Vec<Menu>,
    information_message: String,
}

impl Cell {
    fn new(number: u32) -> Cell {
        Cell {
            number,
            number_text: number.to_string(),
            delta: Vec2::new(0.0, 0.0),
        }
    }

    fn draw(&self, position: Vec2, size: Vec2, color: Color, text_color: Color, spacing: f32) {
        let distance = size + spacing;
        let position = position + self.delta * distance;
        draw_rectangle(position.x, position.y, size.x, size.y, color);

        // draw number text
        let font_size = 42;
        let dims = measure_text(&self.number_text, None, font_size, 1.0);
        let x = position.x + (size.x - dims.width) / 2.;
        let y = position.y + (size.y - dims.height) / 2. + dims.offset_y;
        draw_text(&self.number_text, x, y, font_size as f32, text_color);
    }

    fn is_blank(&self) -> bool {
        self.number == 0
    }
}

impl Board {
    fn draw(&self) {
        for r in 0..ROWS {
            for c in 0..COLS {
                if !self.cells[r][c].is_blank() {
                    self.draw_cell(c, r);
                }
            }
        }
        self.draw_border();
    }

    fn draw_cell(&self, c: usize, r: usize) {
        if let Some(cell_position) = self.position_from_index(c, r) {
            self.cells[r][c].draw(
                cell_position.into(),
                self.cell_size,
                GREEN,
                WHITE,
                self.cell_spacing,
            );
        }
    }

    fn draw_border(&self) {
        let Vec2 { x, y } = self.position;
        let Vec2 { x: w, y: h } = self.size;
        let t = self.border_thickness;
        let color = self.border_color;
        draw_rectangle(x, y, t, h, color); // left
        draw_rectangle(x + w - t, y, t, h, color); // right
        draw_rectangle(x, y, w, t, color); // top
        draw_rectangle(x, y + h - t, w, t, color) // bottom
    }

    fn position_from_index(&self, c: usize, r: usize) -> Option<(f32, f32)> {
        if c < COLS && r < ROWS {
            let (c, r) = (c as f32, r as f32);
            let t = self.border_thickness;
            let s = self.cell_spacing;
            let x = self.position.x + t + c * (s + self.cell_size.x);
            let y = self.position.y + t + r * (s + self.cell_size.y);
            Some((x, y))
        } else {
            None
        }
    }

    fn index_from_position(&self, x: f32, y: f32) -> Option<(usize, usize)> {
        let t = self.border_thickness;
        let left = self.position.x + t;
        let top = self.position.y + t;
        let right = self.position.x + self.size.x - 2. * t;
        let bottom = self.position.y + self.size.y - 2. * t;

        if x >= left && x < right && y >= top && y < bottom {
            let x = (x - self.position.x - t) / self.cell_size.x;
            let y = (y - self.position.y - t) / self.cell_size.y;
            Some((x as usize, y as usize))
        } else {
            None
        }
    }
}

impl SlidingAnimation {
    fn new(speed: f32) -> Self {
        SlidingAnimation {
            col: 0,
            row: 0,
            dir: None,
            speed,
            progress: 0.0,
        }
    }

    fn start(&mut self, col: usize, row: usize, dir: Direction) {
        self.col = col;
        self.row = row;
        self.dir = Some(dir);
        self.progress = 0.0;
    }

    fn update(&mut self, cells: &mut CellArray, delta_time: f32) {
        if self.dir.is_none() {
            return;
        }

        let dir = self.dir.unwrap();

        self.progress += self.speed * delta_time;

        let (c, r) = (self.col, self.row); // short names

        match dir {
            Direction::Left => cells[r][c].delta.x = -self.progress,
            Direction::Right => cells[r][c].delta.x = self.progress,
            Direction::Up => cells[r][c].delta.y = -self.progress,
            Direction::Down => cells[r][c].delta.y = self.progress,
        }

        if self.progress >= 1.0 {
            // finished
            cells[r][c].delta = Vec2::new(0., 0.);
            self.dir = None;
            move_cell(c, r, dir, cells);
        }
    }

    fn is_active(&self) -> bool {
        self.dir.is_some()
    }
}

impl Menu {
    fn new(item: MenuItem, label: &str, position: Vec2, size: Vec2) -> Menu {
        Menu {
            item,
            label: label.to_string(),
            position,
            size,
        }
    }

    fn draw(&self) {
        let fg_color = WHITE;
        let bg_color = GREEN;
        let Vec2 { x, y } = self.position;
        let Vec2 { x: w, y: h } = self.size;
        draw_rectangle(x, y, w, h, bg_color);

        let font_size = h;
        let dims = measure_text(&self.label, None, font_size as u16, 1.0);

        let x = x + (w - dims.width) / 2.;
        let y = y + (h - dims.height) / 2. + dims.offset_y;
        draw_text(&self.label, x, y, font_size, fg_color);
    }

    fn is_point_on_rect(&self, point: Vec2) -> bool {
        let rect = Rect::new(self.position.x, self.position.y, self.size.x, self.size.y);
        rect.contains(point)
    }
}

impl Game {
    fn update(&mut self, delta_time: f32) {
        self.sliding_animation
            .update(&mut self.board.cells, delta_time);

        if !self.sliding_animation.is_active() {
            match self.phase {
                Phase::Playing => self.update_playing(delta_time),
                Phase::Reset => self.update_reset(delta_time),
                Phase::NewGame => self.update_new_game(delta_time),
                Phase::Solve => self.update_solve(delta_time),
                _ => (),
            }
        }

        self.information_message.clear();
        if solved(&self.board.cells) {
            self.information_message.push_str("Solved!");
        } else if self.phase == Phase::Playing {
            self.information_message
                .push_str("Click tile or arrow key to slide");
        } else if self.phase == Phase::NewGame {
            self.information_message
                .push_str("Generating new puzzle...");
        }
    }

    fn update_playing(&mut self, _delta_time: f32) {
        // move cell by mouse click
        if is_mouse_button_pressed(MouseButton::Left) {
            let mpos = mouse_position();
            if let Some((c, r)) = self.board.index_from_position(mpos.0, mpos.1) {
                if let Some(dir) = adjacent_blank_direction(c, r, &self.board.cells) {
                    self.track_of_moves.push(dir);
                    self.sliding_animation.speed = 3.0;
                    self.sliding_animation.start(c, r, dir);
                }
            }
        }

        // move cell by keyboard press
        let (c, r) = find_blank_cell(&self.board.cells);

        let dir = if is_key_pressed(KeyCode::Left) && c < COLS - 1 {
            Some(Direction::Left)
        } else if is_key_pressed(KeyCode::Right) && c > 0 {
            Some(Direction::Right)
        } else if is_key_pressed(KeyCode::Up) && r < ROWS - 1 {
            Some(Direction::Up)
        } else if is_key_pressed(KeyCode::Down) && r > 0 {
            Some(Direction::Down)
        } else {
            None
        };

        if let Some(dir) = dir {
            let (c, r) = index_after_move(c, r, dir.opposite());
            self.track_of_moves.push(dir);
            self.sliding_animation.speed = 3.0;
            self.sliding_animation.start(c, r, dir);
        }
    }

    fn update_reset(&mut self, _delta_time: f32) {
        // rewind track of moves until first player moves
        if self.track_of_moves.len() > self.shuffling_count as usize {
            self.trace_move_back();
        } else {
            self.phase = Phase::Playing;
        }
    }

    fn update_new_game(&mut self, _delta_time: f32) {
        // shuffling board
        if self.shuffling_count < self.shuffling_max {
            let (col, row, dir) =
                next_random_move(&mut self.board.cells, self.track_of_moves.last());
            self.track_of_moves.push(dir);
            self.sliding_animation.speed = 15.0;
            self.sliding_animation.start(col, row, dir);
            self.shuffling_count += 1;
        } else {
            self.phase = Phase::Playing;
        }
    }

    fn update_solve(&mut self, _delta_time: f32) {
        // rewind track of moves
        if !self.track_of_moves.is_empty() {
            self.trace_move_back();
        } else {
            self.phase = Phase::Playing;
        }
    }

    fn handle_menu_click(&mut self) {
        if is_mouse_button_pressed(MouseButton::Left) {
            let mpos = mouse_position();
            for menu in &self.menus {
                if menu.is_point_on_rect(mpos.into()) {
                    self.phase = match menu.item {
                        MenuItem::Reset => Phase::Reset,
                        MenuItem::NewGame => Phase::BrandNewGame,
                        MenuItem::Solve => Phase::Solve,
                    };
                }
            }
        }
    }

    fn trace_move_back(&mut self) {
        if let Some(dir) = self.track_of_moves.pop() {
            let (c, r) = find_blank_cell(&self.board.cells);
            let (c, r) = index_after_move(c, r, dir);
            self.sliding_animation.speed = 15.0;
            self.sliding_animation.start(c, r, dir.opposite());
        }
    }
}

fn adjusted_cell_size(border: f32, spacing: f32, board_size_hint: (f32, f32)) -> (f32, f32) {
    let (cols, rows) = (COLS as f32, ROWS as f32);
    let temp_w = (board_size_hint.0 - 2. * border - (cols - 1.) * spacing) / cols;
    let temp_h = (board_size_hint.1 - 2. * border - (rows - 1.) * spacing) / rows;
    let w = f32::min(temp_w, temp_h);
    let h = w;
    (w, h)
}

fn adjusted_board_size(border: f32, spacing: f32, cell_size: (f32, f32)) -> (f32, f32) {
    let (cols, rows) = (COLS as f32, ROWS as f32);
    let w = 2. * border + (cols - 1.) * spacing + cols * cell_size.0;
    let h = 2. * border + (rows - 1.) * spacing + rows * cell_size.1;
    (w, h)
}

fn setup_board() -> Board {
    let border_thickness = 8.;
    let border_color = BLUE; // Color::new(0.0, 0.0, 1.0, 1.0);
    let cell_spacing = 1.;

    let rate_of_board_to_screen_size = 0.65;
    let size_hint = (
        rate_of_board_to_screen_size * SCR_W as f32,
        rate_of_board_to_screen_size * SCR_H as f32,
    );
    let cell_size = adjusted_cell_size(border_thickness, cell_spacing, size_hint);
    let size = adjusted_board_size(border_thickness, cell_spacing, cell_size);
    let position = ((SCR_W - size.0) / 2., (SCR_H - size.1) / 2.);

    // setup cells
    let mut cells: [[Cell; COLS]; ROWS] = Default::default();
    let mut cell_number = 1;
    for r in 0..ROWS {
        for c in 0..COLS {
            cells[r][c] = if (cell_number as usize) < NUM_CELLS {
                Cell::new(cell_number)
            } else {
                Cell::new(0)
            };
            cell_number += 1;
        }
    }

    Board {
        position: position.into(),
        size: size.into(),
        cell_size: cell_size.into(),
        border_thickness,
        border_color,
        cell_spacing,
        cells,
    }
}

fn adjacent_blank_direction(c: usize, r: usize, cells: &CellArray) -> Option<Direction> {
    if c > 0 && cells[r][c - 1].is_blank() {
        Some(Direction::Left)
    } else if c < COLS - 1 && cells[r][c + 1].is_blank() {
        Some(Direction::Right)
    } else if r > 0 && cells[r - 1][c].is_blank() {
        Some(Direction::Up)
    } else if r < ROWS - 1 && cells[r + 1][c].is_blank() {
        Some(Direction::Down)
    } else {
        None
    }
}

fn move_cell(c: usize, r: usize, dir: Direction, cells: &mut CellArray) {
    let (c2, r2) = index_after_move(c, r, dir);

    let temp = cells[r][c].clone();
    cells[r][c] = cells[r2][c2].clone();
    cells[r2][c2] = temp;
}

fn index_after_move(c: usize, r: usize, dir: Direction) -> (usize, usize) {
    match dir {
        Direction::Left => (c - 1, r),
        Direction::Right => (c + 1, r),
        Direction::Up => (c, r - 1),
        Direction::Down => (c, r + 1),
    }
}

fn next_random_move(
    cells: &mut CellArray,
    last_move: Option<&Direction>,
) -> (usize, usize, Direction) {
    const DIRS: [Direction; 4] = [
        Direction::Left,
        Direction::Right,
        Direction::Up,
        Direction::Down,
    ];

    for r in 0..ROWS {
        for c in 0..COLS {
            if cells[r][c].is_blank() {
                loop {
                    let d = gen_range(0, DIRS.len());
                    if let Some(m) = last_move {
                        if DIRS[d] == *m {
                            // don't turn back
                            continue;
                        }
                    }

                    let ci = c as i32;
                    let ci = match DIRS[d] {
                        Direction::Left => ci - 1,
                        Direction::Right => ci + 1,
                        _ => ci,
                    };

                    let ri = r as i32;
                    let ri = match DIRS[d] {
                        Direction::Up => ri - 1,
                        Direction::Down => ri + 1,
                        _ => ri,
                    };

                    if ci >= 0 && ci < COLS as i32 && ri >= 0 && ri < ROWS as i32 {
                        return (ci as usize, ri as usize, DIRS[d].opposite());
                    }
                }
            }
        }
    }
    panic!("Couldn't find next move");
}

fn find_blank_cell(cells: &CellArray) -> (usize, usize) {
    for r in 0..ROWS {
        for c in 0..COLS {
            if cells[r][c].is_blank() {
                return (c, r);
            }
        }
    }
    panic!("Couldn't find blank cell");
}

fn solved(cells: &CellArray) -> bool {
    let mut n = 1;
    for r in 0..ROWS {
        for c in 0..COLS {
            if n != NUM_CELLS as u32 && cells[r][c].number != n {
                return false;
            }
            n += 1;
        }
    }
    true
}

fn setup_menu() -> Vec<Menu> {
    let w = 130.;
    let h = 30.;
    let bottom = SCR_H - 0.3 * h;
    let x = SCR_W - w - 0.1 * w;
    let y = bottom - 3. * (h + 0.1 * h);
    let reset = Menu::new(MenuItem::Reset, "Reset", Vec2::new(x, y), Vec2::new(w, h));

    let y = bottom - 2. * (h + 0.1 * h);
    let new_game = Menu::new(
        MenuItem::NewGame,
        "New Game",
        Vec2::new(x, y),
        Vec2::new(w, h),
    );

    let y = bottom - (h + 0.1 * h);
    let solve = Menu::new(MenuItem::Solve, "Solve", Vec2::new(x, y), Vec2::new(w, h));

    vec![reset, new_game, solve]
}

fn setup_game() -> Game {
    let phase = Phase::NewGame;
    let board = setup_board();
    let sliding_animation = SlidingAnimation::new(20.0);
    let shuffling_count = 0;
    let shuffling_max = 50;
    let track_of_moves = Vec::new();
    let menus = setup_menu();
    let information_message = String::new();

    Game {
        phase,
        board,
        sliding_animation,
        shuffling_max,
        shuffling_count,
        track_of_moves,
        menus,
        information_message,
    }
}

#[macroquad::main(window_conf)]
async fn main() {
    let mut game = setup_game();

    loop {
        if is_key_pressed(KeyCode::Escape) {
            // quit when Esc key is pressed
            break;
        }

        game.handle_menu_click();

        let delta_time = get_frame_time();
        game.update(delta_time);

        if game.phase == Phase::BrandNewGame {
            game = setup_game();
        }

        clear_background(Color::new(0.0, 0.2, 0.1, 1.0));
        game.board.draw();
        for menu in &game.menus {
            menu.draw();
        }

        draw_text(&game.information_message, 10., 36., 36., WHITE);

        next_frame().await;
    }
}
